-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2015 at 05:21 PM
-- Server version: 5.5.42-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kritzman`
--

-- --------------------------------------------------------

--
-- Table structure for table `adcopies`
--

CREATE TABLE IF NOT EXISTS `adcopies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caseno` int(11) NOT NULL,
  `title` text NOT NULL,
  `body` longtext NOT NULL,
  `part_name` text NOT NULL,
  `feeder_style` text NOT NULL,
  `OBSOLETE` varchar(5) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `directory`
--

CREATE TABLE IF NOT EXISTS `directory` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_phone` varchar(45) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `hours` varchar(255) DEFAULT NULL,
  `img_loc` varchar(255) DEFAULT NULL,
  `description` text,
  `lat` varchar(45) DEFAULT NULL,
  `long` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `directory_cat`
--

CREATE TABLE IF NOT EXISTS `directory_cat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `directory_sub_cat`
--

CREATE TABLE IF NOT EXISTS `directory_sub_cat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `submitter_name` varchar(255) DEFAULT NULL,
  `submitter_phone` varchar(45) DEFAULT NULL,
  `submitter_email` varchar(255) DEFAULT NULL,
  `event_title` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `location_address` varchar(255) DEFAULT NULL,
  `description` text,
  `img_loc` varchar(255) DEFAULT NULL,
  `pending` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_image`
--

CREATE TABLE IF NOT EXISTS `gallery_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `gallery` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `imgloc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `content` text,
  `menukey` int(11) NOT NULL DEFAULT '-1',
  `menuorder` int(11) NOT NULL DEFAULT '0',
  `linkoverride` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT=' ' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`ID`, `title`, `description`, `content`, `menukey`, `menuorder`, `linkoverride`) VALUES
(1, 'HOME', '', '<table align="center" border="0" cellspacing="5" style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;" width="95%">\r\n	<tbody>\r\n		<tr>\r\n			<td align="left" valign="top">\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><span style="color: rgb(255, 0, 0);"><strong><span style="font-size: xx-large;">RE-ELECT</span></strong></span></span></span></p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><span style="color: rgb(255, 0, 0);"><span style="font-size: xx-large;"><span style="color: rgb(0, 0, 128);">Brandon M. Kritzman</span></span></span></span></span></p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><span style="color: rgb(255, 0, 0);"><span style="font-size: xx-large;">Livonia City Council</span></span></span></span></p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><span style="color: rgb(255, 0, 0);"><strong><span style="font-size: xx-large;"><span style="color: rgb(0, 0, 128);"><span style="font-size: small; font-family: arial, helvetica, sans-serif;"><em>&quot;I am very proud of what I have accomplished in the last four years. &nbsp;We must build on these</em></span></span></span></strong></span></span></span></p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><span style="color: rgb(255, 0, 0);"><strong><span style="font-size: xx-large;"><span style="color: rgb(0, 0, 128);"><span style="font-size: small; font-family: arial, helvetica, sans-serif;"><em>great successes and others to ensure that Livonia remains a successful community with</em></span></span></span></strong></span></span></span></p>\r\n\r\n			<p><span style="font-family: arial, helvetica, sans-serif; color: rgb(0, 0, 128); font-size: x-small;"><strong><em><span style="font-size: small;">safe neighborhoods, thriving businesses and an a high quality of life...&quot;</span></em></strong></span></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><span style="color: rgb(255, 0, 0);"><span style="font-size: x-large;"><img alt="Picture of Brandon Kritzman family" height="319" src="http://votekritzman.com/inventory/bk_Family_Pic.jpg" title="family pc" width="435" /></span></span></span></span></p>\r\n\r\n			<p><span style="font-size: medium;"><strong><span style="color: rgb(0, 0, 128);">Brandon and his wife Rebecca with their five beautiful</span></strong></span></p>\r\n\r\n			<p><span style="font-size: medium;"><strong><span style="color: rgb(0, 0, 128);">daughters Jacquelyn, 13, Anastasia, 12, Isabella, 9,</span></strong></span></p>\r\n\r\n			<p><span style="font-size: medium;"><strong><span style="color: rgb(0, 0, 128);">Elizabeth, and Alexandra who is 30 months old!</span></strong></span></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><span style="font-size: large;"><span style="color: rgb(255, 0, 0);"><strong>Brandon&#39;s Goals as Your Councilman:</strong></span></span></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<ul>\r\n				<li><span style="color: rgb(0, 0, 128);"><span style="font-family: arial, helvetica, sans-serif;"><span style="font-size: large;">Preserve Essential City Services</span></span></span></li>\r\n				<li><span style="color: rgb(0, 0, 128);"><span style="font-size: x-large;"><span style="font-size: large;">Ensure Safe &amp; Secure Neighborhoods</span></span></span></li>\r\n				<li><span style="color: rgb(0, 0, 128);"><span style="font-size: x-large;"><span style="font-size: large;">Require Transparent Local Government</span></span></span></li>\r\n				<li><span style="color: rgb(0, 0, 128);"><span style="font-size: x-large;"><span style="font-size: large;">Attract New Business &amp; Support Existing</span></span></span></li>\r\n				<li><span style="color: rgb(0, 0, 128);"><span style="font-size: x-large;"><span style="font-size: large;">Elevate Quality of New Developments</span></span></span></li>\r\n				<li><span style="color: rgb(0, 0, 128);"><span style="font-size: x-large;"><span style="font-size: large;">Continue Fiscally Responsible Budgets</span></span></span></li>\r\n				<li><span style="color: rgb(0, 0, 128);"><span style="font-size: x-large;"><span style="font-size: large;">Invest in User-Friendly Technology</span></span></span></li>\r\n				<li><span style="color: rgb(0, 0, 128);"><span style="font-family: arial, helvetica, sans-serif;"><span style="font-size: large;">Protect Our Most Vulnerable Citizens</span></span></span></li>\r\n			</ul>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><span style="color: rgb(255, 0, 0);"><strong style="font-size: medium;">Election Dates - VOTE!</strong></span></p>\r\n\r\n			<p><span style="font-size: large;"><span style="color: rgb(0, 0, 255);"><strong>Primary Election -&nbsp;August 4, 2015</strong></span></span></p>\r\n\r\n			<p><span style="font-size: large;"><strong><span style="color: rgb(0, 0, 255);">General Election -</span>&nbsp;<span style="color: rgb(0, 0, 255);">November 3, 2015</span></strong></span></p>\r\n			</td>\r\n			<td align="left" valign="top">\r\n			<div>\r\n			<h3 style="color: rgb(0, 51, 102);">Brandon is on Facebook!</h3>\r\n			</div>\r\n\r\n			<div><iframe allowtransparency="true" frameborder="0" scrolling="no" src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FBrandonKritzmanLivoniaCityCouncil&amp;width=292&amp;colorscheme=light&amp;show_faces=true&amp;stream=true&amp;header=false&amp;height=556" style="border-style: none; border-width: initial; width: 292px; height: 556px;"></iframe></div>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td align="left" valign="top">\r\n			<p>&nbsp;</p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><strong>Endorsements</strong></span></span></p>\r\n\r\n			<p><em>While I still feel like a newcomer to local politics, I am proud to have earned the respect and endorsement of established community leaders and will continue work closely with them to keep Livonia a successful city.&nbsp; Endorsements from traditional community leaders, whether in elected office, public life, the business community, education circles or volunteer organizations show how deeply an individual is in tune with the community.&nbsp; I am proud to have such broad based support!!</em></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><em>Please check back for updates to my endrosement list!!</em></p>\r\n\r\n			<table border="0">\r\n				<tbody>\r\n					<tr>\r\n						<td>\r\n						<p><img alt="" height="132" src="http://votekritzman.com/inventory/Mayor.jpg" width="199" /><br />\r\n						<strong><em>Livonia Mayor</em></strong></p>\r\n\r\n						<p><strong><em>Jack Kirksey</em></strong></p>\r\n						</td>\r\n						<td>\r\n						<p><strong><em><img alt="" height="132" src="http://votekritzman.com/inventory/J_McCann_Endorse_Pic.jpg" width="176" /><br />\r\n						Former Council President</em></strong></p>\r\n\r\n						<p><strong><em>James McCann</em></strong></p>\r\n						</td>\r\n					</tr>\r\n					<tr>\r\n						<td>\r\n						<p><br />\r\n						<strong><em><img alt="" height="132" src="http://votekritzman.com/inventory/L_Cox_Endorse_Pic.jpg" width="198" /><br />\r\n						State Representative</em></strong></p>\r\n\r\n						<p><strong><em>Laura Cox</em></strong></p>\r\n						</td>\r\n						<td>\r\n						<p><strong><em><img alt="" height="132" src="http://votekritzman.com/inventory/J_Walsh_Endorse_Pic.jpg" width="198" /><br />\r\n						Former State Representative</em></strong></p>\r\n\r\n						<p><strong><em>John Walsh</em></strong></p>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n\r\n			<p>The following endorsements were from the 2011 election cycle but I plan to pursue them again.</p>\r\n\r\n			<p><strong>LIVONIA POLICE OFFICERS ASSOCIATION&nbsp; -&nbsp;</strong><span style="font-size: small;"><span style="color: rgb(204, 0, 0);"><strong>ENDORSED!!</strong></span></span></p>\r\n\r\n			<p><strong>LIVONIA LIEUTENANTS &amp; SERGEANTS ASSOCIATION -&nbsp;</strong><span style="font-size: small;"><span style="color: rgb(204, 0, 0);"><strong>ENDORSED!!</strong></span></span></p>\r\n\r\n			<p><strong>LIVONIA OBSERVER - Primary &amp; General -&nbsp;</strong><span style="font-size: small;"><span style="color: rgb(204, 0, 0);"><strong>ENDORSED!!</strong></span></span></p>\r\n\r\n			<p><strong>LIVONIA OBSERVER READER PANEL -&nbsp;</strong><span style="font-size: small;"><span style="color: rgb(204, 0, 0);"><strong>ENDORSED!!</strong></span></span></p>\r\n\r\n			<p><strong>RIGHT TO LIFE OF MICHIGAN -&nbsp;</strong><span style="font-size: small;"><span style="color: rgb(204, 0, 0);"><strong>ENDORSED!!</strong></span></span></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong><em>Mark &amp; Brenda LaBerge&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bill Heaton&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dr. Kristal Greniuk</em></strong></p>\r\n\r\n			<p><strong><em>John Hiltz&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Charlie Mahoney&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><strong><em>Kathy Hoen&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</em></strong></p>\r\n\r\n			<p><strong><em>Julie Kain&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><strong><em>Frank Farren&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><strong><em>Mike Mitchell&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong></p>\r\n\r\n			<p><strong><em>George Gostias&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><em><strong>Jim &amp;&nbsp;Char Baringhaus&nbsp;&nbsp;&nbsp;&nbsp;Kim Caruana&nbsp;&nbsp;</strong></em></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><strong>The most important endorsement is YOUR VOTE!</strong></span></span></p>\r\n\r\n			<div id="_mcePaste" style="left: -10000px; top: 0px; width: 1px; height: 1px; overflow: hidden; position: absolute;"><strong><em>Mayor Jack Kirksey</em></strong></div>\r\n			</td>\r\n			<td align="left" valign="top">\r\n			<p style="text-align: center;"><a href="http://www.ci.livonia.mi.us/tabid/164/Departments/City%20Clerk/VoteLivonia/VoteLivoniaHome.aspx" style="color: rgb(215, 24, 40); font-weight: bold; text-decoration: none;" target="_blank"><img alt="City of Livonia" src="http://votekritzman.com/images/livoniaseal.jpg" style="border: 0pt none; margin: 5px;" /></a></p>\r\n\r\n			<p style="text-align: center;">For more information about voting in Livonia, click the city-seal above.</p>\r\n\r\n			<p style="text-align: center;">&nbsp;</p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><strong>Primary Results</strong></span></span></p>\r\n\r\n			<p>There are 16 candidates for CIty Council in 2015.&nbsp; The candidates that receive the 8 highest vote totals in the August 4th election will move on to the general election in November.</p>\r\n\r\n			<p>We will post the election results as soon as they are available.</p>\r\n\r\n			<p>&nbsp;</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td align="left" valign="top">\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><strong>Community Testimonials</strong></span></span></p>\r\n\r\n			<p><span style="font-size: small;">I truly believe Brandon Kritzman is a voice of common sense and unity that will benefit Livonia immensely.&nbsp; I am proud to support him and display a lawn sign in that effort.&nbsp;-&nbsp;<em>Lori Crouson O&#39;Brady</em></span></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><span style="font-size: small;">Brandon has a great vision for Livonia.&nbsp; Thanks for speaking at our Livonia Networking Professionals meeting this week.&nbsp;-&nbsp;<em>Carol Evans, Rodan &amp; Fields</em></span></p>\r\n			</td>\r\n			<td align="left" valign="top">\r\n			<p><span style="font-size: small;"><span style="color: rgb(204, 0, 0);"><strong>&quot;Now is the time for more than just words.&nbsp; We need action with specific goals in mind.&quot;</strong></span></span></p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><strong><span style="font-size: small;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Brandon Kritzman</span></strong></span></span></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, 0, ''),
(2, 'ABOUT', '', '<table align="center" border="0" cellspacing="5" style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;" width="95%">\r\n	<tbody>\r\n		<tr>\r\n			<td align="left" valign="top">\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><strong>Personal Biography</strong></span></span></p>\r\n\r\n			<p>Brandon Kritzman, AIA, is a Senior Architect at OHM Advisors&nbsp;of Livonia, an active community volunteer, traditional family man and homeowner.</p>\r\n\r\n			<p>This diverse and deeply invested background enables Brandon to employ a deeper understanding of the varying points of view the Council represents.</p>\r\n\r\n			<p>Graduating with a Bachelor of Architecture from University of Detroit Mercy, where he met his wife, and subsequently returned to pursue a Master&rsquo;s degree, Brandon is now a registered Architect with the State of Michigan.</p>\r\n\r\n			<p>He has been a visible member of the community and local business environment since the couple moved here, choosing Livonia to raise their five daughters: Jacqui, Ana, Bella, Laney &amp; Allie!</p>\r\n\r\n			<p>Among his many volunteer activities, Kritzman is heavily involved in the Livonia Chamber of Commerce, serving on the Board of Directors and several committees, and was&nbsp;once Ambassador of the Year.</p>\r\n\r\n			<p>He represents the Livonia City Council as the designated delegate to SEMCOG (SouthEast Michigan Council of Governments) and was recently appointed to SEMCOG&#39;s Economic Development Task Force.</p>\r\n\r\n			<p>Livonia faces new problems which require new answers. Just one term in Brandon brings fresh ideas and solutions to Council, balanced with the necessary familiarity, knowledge, passion and experience required to effect changes.</p>\r\n\r\n			<p>Further information is available by e-mailing Kritzman directly at brandon@voteKRITZMAN.com or by calling 313-690-1299.</p>\r\n			</td>\r\n			<td align="left" valign="top">\r\n			<p><strong><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);">List of Qualifications</span></span></strong></p>\r\n\r\n			<ul>\r\n				<li>State of Michigan Licensed Architect #1301056177</li>\r\n				<li>American Institute of Architects</li>\r\n				<li>National Council of Architectural Registration Boards</li>\r\n				<li>LEED AP &ndash; U.S. Green Building Council</li>\r\n				<li>Livonia Chamber of Commerce</li>\r\n				<li>Board of Directors - Since 2009 to Present</li>\r\n				<li>Governmental Affairs Committee</li>\r\n				<li>Ambassador Committee</li>\r\n				<li>2010 Ambassador of the Year</li>\r\n				<li>Livonia Jaycees - 2007-2011</li>\r\n				<li>Board of Directors &ndash; 2008-2010</li>\r\n				<li>VP of Communication 2008</li>\r\n				<li>VP of Community Development 2009</li>\r\n				<li>Board Member-at-Large 2010</li>\r\n				<li>Livonia Family YMCA</li>\r\n				<li>Board of Advisors &ndash; Since 2010 to 2012</li>\r\n				<li>St. Michael the Archangel Catholic Church, Livonia</li>\r\n				<li>ORGHA Soccer Coach,&nbsp;6 Years</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td align="left" valign="top">\r\n			<p>&nbsp;</p>\r\n\r\n			<p><span style="font-size: large;"><strong><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);">Brandon in the News&nbsp; (All good things!!)</span></span></strong></span></p>\r\n\r\n			<p><strong>As the endorsements have not come out yet, I thought I would share some recent articles regarding Council matters and some of my thoughts on the subject.</strong></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong><strong>...from the Livonia Observer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: rgb(255, 0, 0);">&nbsp;</span></strong></strong></p>\r\n\r\n			<p><strong><span style="font-size: small;">Livonia City Council&nbsp; to vote on proposed hotel June 1&nbsp;&nbsp;</span></strong><span style="font-size: small;"><span style="font-size: x-small;">May 21</span><span style="font-size: x-small;">, 2015</span></span></p>\r\n\r\n			<p>The Livonia City Council will make a decision early next month on whether to approve the site plan and other conditions for the proposed hotel on the Livonia/Redford border.</p>\r\n\r\n			<p>...Kritzman said while the building&#39;s stone base is an appealing touch, he would have liked to have seen more exploration of the using brick in the materials.</p>\r\n\r\n			<p>&quot;Whether it&#39;s psychological or visual, it is going to be an entrance to the city,&quot; Kritzman said.&nbsp; &quot;That&#39;s why I think it does require a little extra discretion on our part to make certain that this building is where we want it to be from a materials standpoint.&quot;</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong><strong>...from the Livonia Observer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: rgb(255, 0, 0);">&nbsp;</span></strong></strong><span style="font-size: small;"><strong><span style="color: rgb(204, 0, 0);"><strong>ENDORSED!!</strong></span></strong></span></p>\r\n\r\n			<p><strong><span style="font-size: small;">Kritzman offers needed solutions*&nbsp; (from the Livonia Observer Reader Panel!)</span></strong></p>\r\n\r\n			<p><span style="font-size: x-small;">Brandon Kritzman brings the perspective of a Chamber of Commerce board member, an architect and father. A local businessman, he understands those challenges and the need to be proactive in retaining existing businesses. He supports developing a 10-year strategic plan to put Livonia on the path to a shared vision of the future, and his architectural skills will undoubtedly help generate ideas for Livonia&#39;s future.</span></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><span style="font-size: x-small;">A representative of Livonia&#39;s young families, Kritzman wants to link city attractions and create new destination points that will appeal to young adults. He also supports investing in user-friendly<a href="http://www.hometownlife.com/apps/pbcs.dll/article?AID=2011110270626" id="KonaLink4" style="color: rgb(215, 24, 40); font-weight: bold; position: static;" target="undefined"><span style="color: blue; font-weight: 400; font-size: 16px; position: static;"><span style="font-family: Helvetica, Arial, sans-serif; position: static;">&nbsp;</span></span></a>technology, such as online permits, to engender long-term savings and make it easier to conduct business with the city.</span></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong><strong>...from the Livonia Observer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: rgb(255, 0, 0);">&nbsp;</span></strong></strong><span style="font-size: small;"><strong><span style="color: rgb(204, 0, 0);"><strong>ENDORSED!!</strong></span></strong></span></p>\r\n\r\n			<p><strong><span style="font-size: small;">Aug 2: Choose Knowledgable Candidates</span></strong></p>\r\n\r\n			<p>Brandon Kritzman brings the perspective of a Chamber of Commerce board member, an architect and father. A local businessman, he understands those challenges and the need to be proactive in retaining existing businesses. He supports developing a 10-year strategic plan to put Livonia on the path to a shared vision of the future, and his architectural skills will undoubtedly help generate ideas for Livonia&#39;s future.</p>\r\n\r\n			<p>A representative of Livonia&#39;s young families, Kritzman wants to link city attractions and create new destination points that will appeal to young adults. He also supports investing in user-friendly technology, such as online permits, to engender long-term savings and make it easier to conduct business with the city.</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>...from the Livonia Observer</strong></p>\r\n\r\n			<p><strong><span style="font-size: small;">Architect files to run for council</span></strong></p>\r\n\r\n			<p>A local architect has filed to run for a seat on Livonia City Council.</p>\r\n\r\n			<p><strong>Brandon Kritzman</strong>, owner of New Perspective Architects, submitted a candidate application with the City Clerk&#39;s office Tuesday to officially begin his campaign.</p>\r\n\r\n			<p>Kritzman is involved in the Livonia Chamber of Commerce, serving on the board of directors and several committees, and was recently recognized as Ambassador of the Year. He also serves on the board of the Livonia Family YMCA and is a member of the Livonia Jaycees.</p>\r\n\r\n			<p>Kritzman obtained a bachelor of architecture degree from the University of Detroit Mercy in 1999 and subsequently returned to pursue a master&#39;s degree. Kritzman is a registered architect and LEED-accredited professional.</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>...from the Livonia Observer</strong></p>\r\n\r\n			<p><strong><span style="font-size: x-small;"><span style="font-size: small;">Cha</span><span style="font-size: small;">mber to honor businesses of the year</span></span></strong></p>\r\n\r\n			<p>The Chamber also will honor&nbsp;<strong>Brandon Kritzman</strong>&nbsp;as its Ambassador of the Year, which is awarded to the Chamber&#39;s most active volunteer over the past year. In addition to helping recruit new members, welcome new business owners at grand openings, and provide support at Chamber events, Kritzman has been instrumental in helping the Chamber develop a new event, the Livonia Home Improvement Show, set for March 26.</p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong>...from the Livonia Observer</strong></p>\r\n\r\n			<p><strong><span style="font-size: small;">Awards program honors Livonia&#39;s best</span></strong></p>\r\n\r\n			<p>Livonia&#39;s top volunteers, teachers and business leaders were honored Thursday during the seventh annual Leadership and Awards Celebration presented by the Livonia Chamber of Commerce and the Observer &amp; Eccentric Newspapers.</p>\r\n\r\n			<p><strong>Brandon Kritzman</strong>&nbsp;of New Perspectives Architects was honored with the chamber&#39;s Ambassador of the Year award. He is helping to develop the chamber&#39;s new Home Improvement Show that will be held in March and serves on the chamber&#39;s board and government affairs committee, among other chamber duties.</p>\r\n			</td>\r\n			<td align="left" valign="top">\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><strong>&nbsp;</strong></span></span></p>\r\n\r\n			<p><span style="font-size: medium;"><span style="color: rgb(204, 0, 0);"><strong>Letters to the Editor</strong></span></span></p>\r\n\r\n			<p><span style="font-size: small;">Vote Kritzman for council</span></p>\r\n\r\n			<p><span style="font-size: small;">On Tuesday, Aug. 2, I will be casting a vote for Brandon Kritzman to serve on the Livonia City Council. I urge all voters in Livonia to do likewise.</span></p>\r\n\r\n			<p><span style="font-size: small;">I have spent the last several years volunteering with Brandon during our involvement with the Livonia Chamber of Commerce and have developed a sincere respect for him and his desire to provide the proper structure for Livonia&#39;s future.</span></p>\r\n\r\n			<p><span style="font-size: small;">Livonia has a great foundation that was put in place over 60 years ago, producing the first-class city that our families enjoy. However, we can&#39;t stop there. We can&#39;t settle. The unfortunate state of the economy is forcing us to reconsider and reconstruct our city, both figuratively and literally. It&#39;s not going to be easy, but Brandon is equipped with the capabilities needed to move Livonia forward while respecting our city&#39;s history and the work of previous generations.</span></p>\r\n\r\n			<p><span style="font-size: small;">Fortunately, as an architect licensed by the state of Michigan, Brandon Kritzman routinely prepares complex projects using his creative problem-solving skills. He works daily at his local business, with complicated codes and laws to design structures in our community and surrounding areas that will be in place for many years. When elected to the City Council, all citizens will have the opportunity to see for themselves the strategic vision he possesses, utilizing common sense ideas, creativity and energy to positively affect Livonia.</span></p>\r\n\r\n			<p><span style="font-size: small;">During these difficult economic times when many of our young people are moving out of Michigan after falling victim to the American Dream in reverse, Brandon Kritzman and his family are a prime example of what we need in our city. After college, Brandon and Rebecca chose Livonia as the community in which to buy a home and raise their family. They are now the proud parents of four young daughters. He values their decision to plant their roots here and it shows through his community involvement and dedication to Livonia. I am confident that Brandon will continue to work tirelessly to make sure Livonia is the best city to live, work and play.</span></p>\r\n\r\n			<p><span style="font-size: small;">It&#39;s easy to hold elected office when times are good &mdash; let&#39;s make sure that we have the right elected officials to lead us when times are not so good. I believe that Brandon Kritzman will bring a new perspective while serving us on the Livonia City Council.</span></p>\r\n\r\n			<p><span style="font-size: small;">Vote Brandon Kritzman for Livonia City Council.</span></p>\r\n\r\n			<p><span style="font-size: small;">-&nbsp;&nbsp;<em>Mark LaBerge, Livonia</em></span></p>\r\n\r\n			<p><span style="font-size: x-small;">&nbsp;</span></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, 0, ''),
(3, 'ISSUES', '', '<table align="center" border="0" cellspacing="5" style="font-family: Verdana, Geneva, sans-serif; font-size: 12px;" width="95%">\r\n	<tbody>\r\n		<tr>\r\n			<td align="left" valign="top">\r\n			<p><strong><span style="color: rgb(204, 0, 0);"><span style="font-size: medium;">Goals as a Council Member</span></span></strong></p>\r\n\r\n			<p><strong>Fiscal Responsibility</strong></p>\r\n\r\n			<p><em>Brandon will fight to make every tax dollar collected from our citizens as effective as it can be.</em></p>\r\n\r\n			<ul>\r\n				<li>Continually refine city services while protecting our most vulnerable citizens</li>\r\n				<li>Invest in technology capable of yielding long term savings and efficiencies</li>\r\n				<li>Ensure that the responsibility for the solution must be shared by everyone</li>\r\n			</ul>\r\n\r\n			<p><strong>Transparency in Government</strong></p>\r\n\r\n			<p><em>Livonia owes its citizens a clear and simple picture of how our tax dollars are spent</em></p>\r\n\r\n			<ul>\r\n				<li>Develop a &lsquo;Livonia Dashboard&rsquo; for clear and simple review of city finances</li>\r\n				<li>Implement accountability metrics to track effectiveness at all levels</li>\r\n				<li>Make available a searchable Real Time Checkbook on the city web site</li>\r\n			</ul>\r\n\r\n			<p><strong>Economic Enhancement</strong></p>\r\n\r\n			<p><em>We must commit to sharing Livonia&rsquo;s Great Attributes with a wider business and residential audience</em></p>\r\n\r\n			<ul>\r\n				<li>Greater emphasis on retention and support of existing businesses</li>\r\n				<li>Provide dedicated resources to proactively market and promote Livonia</li>\r\n				<li>Streamline business interaction with City Hall to be more investment friendly</li>\r\n			</ul>\r\n\r\n			<p><strong>Invest in Technology</strong></p>\r\n\r\n			<p><em>Livonia must invest in emerging technology to save money and provide more efficient services to citizens</em></p>\r\n\r\n			<ul>\r\n				<li>Develop up to date online &quot;Forms&quot; that can be submitted via the internet</li>\r\n				<li>Re-Invent the city web site to be more user friendly and capable of serving citizens</li>\r\n				<li>Utilize time saving QR codes to communicate messages from inspectors and departments</li>\r\n			</ul>\r\n\r\n			<p><strong>Develop Strategic Plan</strong></p>\r\n\r\n			<p><em>Livonia needs a ten year strategic plan to prepare our city for future generations and capital investment</em></p>\r\n\r\n			<ul>\r\n				<li>Work with community leaders and citizens to envision a future model of Livonia</li>\r\n				<li>Conduct a series of workshops to explore concerns and actually DO something about it!</li>\r\n				<li>At some point capital investment will return and we need a plan not to waste citizens money</li>\r\n			</ul>\r\n			</td>\r\n			<td align="left" valign="top">\r\n			<p><strong><span style="color: rgb(204, 0, 0);"><span style="font-size: medium;">Looking for more?</span></span></strong></p>\r\n\r\n			<p><strong>League of Women Voters</strong></p>\r\n\r\n			<p><em>The LWV of Northwest Wayne County issues a &quot;Voter Guide&quot; for you to learn more.&nbsp; The link below will take you there to learn more about each of the candidates.&nbsp; They plan to issue video clips of each primary election candidate which should be available below.</em></p>\r\n\r\n			<ul>\r\n				<li><a href="http://www.lwvnww.org/" style="color: rgb(215, 24, 40); font-weight: bold; text-decoration: none;">http://www.lwvnww.org</a></li>\r\n			</ul>\r\n\r\n			<p><strong>Livonia Chamber of Commerce</strong></p>\r\n\r\n			<p><em>The Livonia Chamber of Commerce has traditionally been a great resource of information regarding local elections.</em></p>\r\n\r\n			<ul>\r\n				<li><a href="http://www.livonia.org/candidates.asp" style="color: rgb(215, 24, 40); font-weight: bold; text-decoration: none;">http://www.livonia.org</a></li>\r\n			</ul>\r\n\r\n			<p><em>Please spend some additional time on the Chamber&#39;s web site learning more about our local business community.</em></p>\r\n\r\n			<ul>\r\n				<li><a href="http://www.livonia.org/" style="color: rgb(215, 24, 40); font-weight: bold; text-decoration: none;">http://www.livonia.org</a></li>\r\n			</ul>\r\n\r\n			<p><strong>Livonia Observer&nbsp;&nbsp; Hometownlife.com</strong></p>\r\n\r\n			<p><em>The online home of the Livonia Observer, hometownlife.com offers ongoing covergae including the voter guide below with each candidates answers to a series of questions.</em></p>\r\n\r\n			<ul>\r\n				<li><a href="http://www.hometownlife.com/section/NEWS10" style="color: rgb(215, 24, 40); font-weight: bold; text-decoration: none;">www.hometownlife.com</a></li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td align="left" valign="top">\r\n			<p><strong><span style="color: rgb(204, 0, 0);"><span style="font-size: medium;">Often Asked Questions</span></span></strong></p>\r\n\r\n			<p><strong><em>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><strong><em>Why are you running for office? What do you hope to accomplish?</em></strong></p>\r\n\r\n			<p>As a homeowner, local business owner, active community volunteer and traditional family man I am completely invested in Livonia&rsquo;s future.&nbsp;</p>\r\n\r\n			<p>As a father of four young daughters, I am interested in solving today&rsquo;s problem while shaping what Livonia will be in ten years. I want to make certain for my girls, that the Livonia they grow up in is just as great as ever.&nbsp;</p>\r\n\r\n			<p>Livonia&rsquo;s future must be founded in attracting new, young families to fill our neighborhoods, excel in our schools and support our local businesses.&nbsp; I represent a fresh presence balanced with experience, new ideas and passion for Livonia.</p>\r\n\r\n			<p><strong><em>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><strong><em>What is the role of a city council person?</em></strong></p>\r\n\r\n			<p>Elected officials are meant to serve as representatives of the people. The daily responsibilities of council, to shape the budget, authorize expenses, handle legislative duties, guide future efforts and provide checks and balances to the Mayor&rsquo;s office must represent the voice of the citizens.&nbsp;</p>\r\n\r\n			<p>As this economy has forced each of us to revise the way we do things in our home and our lives, our officials should be doing the same thing with our tax dollars. The role of a city council person is to bring innovative ideas to the city operations, ensuring the provision of services within our means.</p>\r\n\r\n			<p><strong><em>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><strong><em>What are the top three issues the council should address in the coming term?</em></strong></p>\r\n\r\n			<p>The top three issues that must be addressed are preparing for future budget shortfalls, enacting structural reform of City Hall operations, and developing a strategic long term plan for Livonia.</p>\r\n\r\n			<p>Since tax revenue is tied to property values, it will take years for the city&rsquo;s revenue to recover to previous levels.&nbsp; Now is the time that Livonia must shed its excesses and work hard to become a leaner, more efficient example of local government.&nbsp; Merely getting by or even solving today&rsquo;s problems without planning for our future will ensure that we end up right back where we started.</p>\r\n\r\n			<p><strong><em>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><strong><em>How should the council address those issues?</em></strong></p>\r\n\r\n			<p>Livonia is too dependent on antiquated systems that create a cumbersome, slow and costly environment within City Hall.&nbsp; Investing in user-friendly technology that will result in long term efficiencies and savings will yield a smaller, faster, more efficient and more cost effective local government.&nbsp;</p>\r\n\r\n			<p>These structural changes will reduce costs, leaving more resources within the budget.&nbsp; Shortfalls and liabilities must be aggressively projected so that solutions can be found early, without asking the citizens to shoulder the burden with their wallets.</p>\r\n\r\n			<p>These changes will take time to implement, but with long term planning, will result in better service to the citizens.</p>\r\n\r\n			<p><strong>7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><strong><em>What services should be looked at in the budget to be cut? What should be consolidated or regionalized</em></strong><strong>?</strong></p>\r\n\r\n			<p>It&rsquo;s a shame that the first solution offered is to &lsquo;cut&rsquo; something.&nbsp; What we should be concentrating on is finding ways to preserve the services and programs that have become part of Livonia&rsquo;s quality of life.&nbsp; People always push to protect the service they use but are quick to cut those they don&rsquo;t.&nbsp;</p>\r\n\r\n			<p>A successful city is one that provides appropriate services for all its citizens.&nbsp; Everyone in this city is important. In regards to the budget, those services that can be consolidated, regionalized or even privatized to produce a cost savings without losing quality, institutional knowledge or local control should be considered.</p>\r\n\r\n			<p><strong><em>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</em></strong><strong><em>What do you think of how the city handled the flooded basement issue?</em></strong></p>\r\n\r\n			<p>I believe that the city did some things quite well which should be recognized, but made crucial mistakes as well, most notably on the communications side.&nbsp;</p>\r\n\r\n			<p>In an age where automated phone calls, digital alerts and e-mail notifications are so easy to set up and manage, Livonia needs to do more to keep citizens informed in events like this.&nbsp; A simple automated phone call could have avoided much of the loss, incurred expenses and exposure to unhealthy conditions that we experienced.&nbsp; Tell us when to worry about sewer backups.&nbsp; Tell us that trucks are coming.&nbsp; Tell us that there is help.</p>\r\n			</td>\r\n			<td align="left" valign="top">\r\n			<p><strong><span style="color: rgb(204, 0, 0);"><span style="font-size: medium;">Do You Have Questions?</span></span></strong></p>\r\n\r\n			<p><em><strong>I would love to hear from you.&nbsp; I believe that our local elected officials need to be more accessible to people.&nbsp; From the very start of my cfirst ampaign I have put an email, phone number, facebook and web site on each piece of literature.&nbsp; If you would like to talk to me before you give me your support, please do not hesitate to contact me.</strong></em></p>\r\n\r\n			<p>&nbsp;</p>\r\n\r\n			<p><strong><span style="color: rgb(204, 0, 0);"><span style="font-size: medium;">Contact Brandon...</span></span></strong><span style="font-size: medium;"><span style="color: rgb(255, 0, 0);">&nbsp;</span></span></p>\r\n\r\n			<p><strong><em>Phone:</em></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 313-690-1299</p>\r\n\r\n			<p><strong><em>Email:</em></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; brandon@voteKRITZMAN.com</p>\r\n\r\n			<p><em><strong>Web:</strong></em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; www.voteKRITZMAN.com</p>\r\n\r\n			<p><em><strong>Facebook:</strong></em>&nbsp; News from the Livonia City Council - Brandon M Kritzman</p>\r\n\r\n			<p><em><strong>Address:</strong></em>&nbsp;&nbsp;&nbsp;&nbsp; Citizens for Brandon M. Kritzman</p>\r\n\r\n			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 29601 Jacquelyn Drive</p>\r\n\r\n			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Livonia, MI 48154</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 0, 0, ''),
(4, 'CONTACT', '', '', 0, 0, '../../contact/'),
(5, 'GET INVOLVED', '', '', 0, 0, '../../getinvolved/');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `content` text,
  `menukey` int(11) NOT NULL DEFAULT '-1',
  `menuorder` int(11) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' ' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
