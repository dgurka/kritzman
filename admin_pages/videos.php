<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$menus = Db::ExecuteQuery("SELECT * FROM videos WHERE menukey = 0 ORDER BY ID", $conn);

$vidmenu = "<ul>";

foreach ($menus as $value) 
{
	
	$menukey = $value["ID"];
	//$vids = Db::ExecuteQuery("SELECT ID, title FROM videos WHERE menukey = $menukey ORDER BY menuorder DESC, ID", $conn);
	
	$vidmenu .= "<li>" . $value["title"] . "  <button class='btn' onclick='editVideo(".$value['ID'].")'>edit</button></li>"; 
	
	//$vidmenu .= " <button class='btn' onclick='moveUp(".$value['ID'].")'>Up</button>";

	//$vidmenu .= " <button class='btn' onclick='moveDown(".$value['ID'].")'>Down</button>";

}

$vidmenu .= "</ul>";

$vids = Db::ExecuteQuery("SELECT ID, title FROM videos WHERE menukey = -1 ORDER BY ID", $conn);

if(count($vids))
{
	$vidmenu .= "<h3>Hidden Videos</h3><ul>";
	foreach ($vids as $i => $vid) 
	{
		$vidmenu .= "<li>" . $vid["title"];
		$vidid = $vid["ID"];

		$vidmenu .= " <button class='btn' onclick='editVideo($vidid)'>edit</button>";

		$vidmenu .= "</li>";
	}

	$vidmenu .= "</ul>";
}


$context["pagemenu"] = $vidmenu;
$context["HAS_MAIN_PAGE"] = MAIN_PAGE == "";

echo $twig->render('videos.html', $context);