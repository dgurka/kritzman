<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$gals = array();

foreach (explode(",", GALLERY_NAMES) as $key => $value) 
{
	$gals[] = array(
		"id" => $key,
		"value" => $value
	);
}

$context["gals"] = $gals;

echo $twig->render('galleries.html', $context);