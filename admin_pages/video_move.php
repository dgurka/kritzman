<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$id = (int)$matches[1];

$menukey = Db::ExecuteFirst("SELECT menukey FROM videos WHERE ID = $id", $conn);

$menukey = $menukey["menukey"];

$videos = Db::ExecuteQuery("SELECT ID FROM videos WHERE menukey = $menukey ORDER BY menuorder DESC, ID", $conn);

/*web_var_dump($videos);
exit();*/

$len = count($videos);
for ($i=0; $i < $len; $i++) 
{ 
	if($videos[$i]["ID"] == $id)
	{
		if($matches[2] == "up")
		{
			$tmp = $videos[$i];
			$nkey = (int)$i-1;
			$videos[$i] = $videos[$nkey];
			$videos[$nkey] = $tmp;
		}
		else
		{
			$tmp = $videos[$i];
			$nkey = (int)$i+1;
			$videos[$i] = $videos[$nkey];
			$videos[$nkey] = $tmp;
		}
		break;
	}
}

for ($i=0; $i < count($videos); $i++) 
{
	$place = (count($videos) - $i);
	$_id = $videos[$i]["ID"];
	Db::ExecuteNonQuery("UPDATE videos SET menuorder = $place WHERE ID = $_id", $conn);
}
redirect(URL_ROOT . "admin/videos/");