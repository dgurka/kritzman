<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$title = Db::EscapeString(post("title"), $conn);
	$menukey = Db::EscapeString(post("menukey"), $conn);
	$linkoverride = Db::EscapeString(post("linkoverride"), $conn);
	$description = Db::EscapeString(post("description"), $conn);
	$content = Db::EscapeString(post("content"), $conn);

	if(isset($matches[1]))
	{
		$id = (int)$matches[1];

		$query = "UPDATE page SET title = '$title', menukey = '$menukey', linkoverride = '$linkoverride', description = '$description', content = '$content' WHERE ID = $id";

		Db::ExecuteNonQuery($query, $conn);
		redirect(URL_ROOT . "admin/page/" . $id . "/");
	}
	else
	{
		$query = "INSERT INTO page (title, menukey, linkoverride, description, content) VALUES ('$title', '$menukey', '$linkoverride', '$description', '$content')";

		Db::ExecuteNonQuery($query, $conn);
		$id = Db::GetLastInsertID($conn);
		redirect(URL_ROOT . "admin/page/" . $id . "/");
	}

	exit();
}

if(isset($matches[1]))
{
	$id = (int)$matches[1];
	$context["id"] = $id;
	$page = Db::ExecuteFirst("SELECT * FROM page WHERE ID = $id", $conn);
	$title = str_replace("\"", "&quot;", $page["title"]);
	$menukey = $page["menukey"];
	$linkoverride = $page["linkoverride"];
	$description = $page["description"];
	$content = $page["content"];
}
else
{
	$title = "";
	$menukey = -1;
	$linkoverride = "";
	$description = "";
	$content = "";
}

$menuselect = array();

if($menukey == "-1"){
	$menuselect[] = "<option value='-1' selected='selected'>Not in menu</option>";
	$menuselect[] = "<option value='0'>Top Level Page</option>";

} else if($menukey == "0"){
	$menuselect[] = "<option value='-1'>Not in menu</option>";
	$menuselect[] = "<option value='0' selected='selected'>Top Level Page</option>";
} else {
	$menuselect[] = "<option value='-1'>Not in menu</option>";
	$menuselect[] = "<option value='0'>Top Level Page</option>";	
}


//$menuitems = Db::ExecuteQuery("SELECT * FROM menu ORDER BY ID", $conn);
$menuitems = Db::ExecuteQuery("SELECT * FROM page WHERE menukey = 0 OR menukey = -1 ORDER BY  menuorder ASC, ID", $conn);

/*
This method works but generates an interested result:
*all* pages are listed then *all* have their subpages listed, 
so "Videos" shows up under "About", and then again at the bottom as its own entry, with all its subpages

As stated above this function allows the user to see every page, but the nesting may be a bit confusing

Another option may be to simply list all the pages without any nesting? 

*/

foreach ($menuitems as $value) 
{
	$mid = $value["ID"];
	$mname = $value["title"];
	$m = "<option value=\"$mid\" title=\"$mid\"";
	if($mid == $menukey){
		$m .= " selected='selected'";
	}
	$m .= ">&bull; $mname</option>";


	$menuselect[] = $m;
	
	
	// This code generates a list of pages under a certain menu key
	$submenuitems = Db::ExecuteQuery("SELECT ID, title FROM page WHERE menukey = ".$mid." ORDER BY menuorder DESC, ID", $conn);
	foreach ($submenuitems as $value) 
	{
		$pid = $value["ID"];
		$pname = $value["title"];
		$p = "<option value=\"".$pid."\" title=\"$pid\"";
		if($pid == $menukey){
			$p .= " selected='selected'";
		}
		$p .= ">&mdash; $pname</option>";
	
		$menuselect[] = $p;
		
		$subsubmenuitems = Db::ExecuteQuery("SELECT ID, title FROM page WHERE menukey = ".$pid." ORDER BY menuorder DESC, ID", $conn);
		foreach ($subsubmenuitems as $svalue) 
		{
			$spid = $svalue["ID"];
			$spname = $svalue["title"];
			$sp = "<option value=\"".$spid."\" title=\"$spid\"";
			if($spid == $menukey){
				$sp .= " selected='selected'";
			}
			$sp .= ">&mdash;&mdash; $spname</option>";
		
			$menuselect[] = $sp;
		}
	}

}

$context["title"] = $title;
$context["linkoverride"] = $linkoverride;
$context["description"] = $description;
$context["content"] = $content;
$context["menuselect"] = implode("", $menuselect);

echo $twig->render('page.html', $context);