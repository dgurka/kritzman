<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$dispmsg = "";

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$title = Db::EscapeString(post("title"), $conn);
	$menukey = Db::EscapeString(post("menukey"), $conn);
	$link = Db::EscapeString(post("link"), $conn);
	$description = Db::EscapeString(post("description"), $conn);
	$content = Db::EscapeString(post("content"), $conn);
	$dispmsg = "Your changes have been saved.";
	$heading = "Edit";

	
	if(isset($matches[1]))
	{
		$id = (int)$matches[1];

		$query = "UPDATE videos SET title = '$title', menukey = '$menukey', link = '$link', description = '$description', content = '$content' WHERE ID = $id";

		Db::ExecuteNonQuery($query, $conn);
		redirect(URL_ROOT . "admin/video/" . $id . "/");
	}
	else
	{
		$query = "INSERT INTO videos (title, menukey, link, description, content) VALUES ('$title', '$menukey', '$link', '$description', '$content')";

		Db::ExecuteNonQuery($query, $conn);
		$id = Db::GetLastInsertID($conn);
		redirect(URL_ROOT . "admin/video/" . $id . "/");
	}

	exit();
}

if(isset($matches[1]))
{
	$id = (int)$matches[1];
	$context["id"] = $id;
	$vid = Db::ExecuteFirst("SELECT * FROM videos WHERE ID = $id", $conn);
	$title = str_replace("\"", "&quot;", $vid["title"]);
	$menukey = $vid["menukey"];
	$link = $vid["link"];
	$description = $vid["description"];
	$content = $vid["content"];
	$heading = "Edit";
	$dispmsg = "";
}
else
{
	$title = "";
	$menukey = -1;
	$link = "";
	$description = "";
	$content = "";
	$heading = "Add";
	$dispmsg = "";
}

$context["title"] = $title;
$context["link"] = $link;
$context["description"] = $description;
$context["content"] = $content;

$context["heading"] = $heading;
$context["dispmsg"] = $dispmsg;

$context["enabled"] = "";
$context["disabled"] = "";

if($menukey == "0"){
	$context["enabled"] = "checked";
} else {
	$context["disabled"] = "checked";
}

echo $twig->render('video.html', $context);