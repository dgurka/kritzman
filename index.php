<?php
 
/*$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;*/

// set this first!
date_default_timezone_set('America/Detroit');

session_start();
function myfunction(){
	
}
// this is some notes
// these are some other notes


require_once("defines.php");
require_once(BASE_DIR . "includes/error_handling.php");
require_once(BASE_DIR . "includes/db.php");
require_once(BASE_DIR . "includes/functions.php");
require_once(BASE_DIR . "includes/Cache.php");
require_once("uri_matching.php");


$m = new URIMatcher();
$page = $m->getPage($matches);

// if it's not found go to 404
if($page === false)
	$page = "404";

$page_parts = explode(":", $page);
if(count($page_parts) == 1)
	$pageloc = "pages/" . $page;
else
	$pageloc = implode("/", $page_parts);

$pageloc .= ".php";

if(file_exists(BASE_DIR . $pageloc))
{
	// please be aware that $matches can still be accessed by the page as long as the page isn't in a function
	require_once(BASE_DIR . $pageloc);
}
else
{
	echo $pageloc;
	
	web_var_dump($matches);
}