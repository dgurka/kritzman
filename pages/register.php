<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$name = post("name");
	$email = post("email");
	$message = post("message");
	
	$body = "";			
	$body .= "organization name: " . $_POST["business_name"] . "\n";
	$body .= "mailing address: " . $_POST["mailing_address"] . "\n";
	$body .= "physical address: " . $_POST["physical_address"] . "\n";
	$body .= "contact name: " . $_POST["contact_name"] . "\n";
	$body .= "telephone: " . $_POST["telephone"] . "\n";
	$body .= "email: " . $_POST["email"] . "\n";
	$body .= "url: " . $_POST["url"] . "\n";

	$subject = "webinar request";
				
	$sendmail = mail("dgurka@enablepoint.com", $subject, $body, "From: no-reply@enablepoint.com");
	$sendmail2 = mail("frank@enablepoint.com", $subject, $body, "From: no-reply@enablepoint.com");
	
	
	//if(mail(ADMIN_EMAIL, "Contact Request", $body, "From: " . NO_REPLY_EMAIL ))
	if($sendmail)
	{
		$context["message"] = "Thank you for your interest. A webinar representative will be contacting you soon.";
	}
	else
	{
		$context["message"] = "An error has occured, please try again.";
	}
}

echo $twig->render('register.html', $context);