<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$cat = (int)$matches[1];

if($cat == -1 || !$cat || $cat == "0"){
	$header = GENERAL_EVENT_NAME;
} else {
	$cats = explode(",", EVENT_CATEGORIES);
	$header = $cats[$cat];
}

$events = Db::ExecuteQuery("SELECT * FROM event WHERE type = $cat AND pending = 0 ORDER BY event_title", $conn);

Db::CloseConnection($conn);

$context["header"] = $header;
$context["events"] = $events;

echo $twig->render('events.html', $context);