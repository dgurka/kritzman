<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();	 

$casestudies = Db::ExecuteQuery("SELECT id,caseno,title,OBSOLETE FROM  `adcopies` ORDER BY id", $conn);


	$renderpage = "<br />"; // escape first line
	$renderpage .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" valign=\"top\">";

	foreach ($casestudies as $i => $cs) 
	{
		if($cs['id'] == "66"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		if($cs['id'] == "131"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		if($cs['id'] == "196"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		// is the ad copy out of date?
		if($cs['OBSOLETE'] == "yes"){ //yup. don't display a link to it.
			$renderpage .= $cs['caseno'] . " - ". $cs['title'] . "<br />";
		} else if($cs['OBSOLETE'] == "no"){ // nope, its still good. display a link.
			$renderpage .= $cs['caseno'] . " - <a href=\"../../adcopy/".$cs['id']."\">". $cs['title'] . "</a><br />";
		}
		
	}

	$renderpage .= "</td></tr></table>";



$context["body"] = $renderpage;

echo $twig->render('casestudies.html', $context);