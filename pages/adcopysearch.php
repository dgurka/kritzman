<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$message = "Please enter a search term, for example, &quot;bolt&quot;";

if($_SERVER["REQUEST_METHOD"] == "POST"){
	
	$message = "";
	
	// Get the search variable from URL
	
	$var = @$_POST['query'];
	$trimmed = trim($var); //trim whitespace from the stored variable
	
	if ($trimmed == "" OR !isset($var)){	// check for an empty string and display a message.
		$message = "You have not entered a search term. Displaying all listings.";
	} 
	
	$conn = Db::GetNewConnection();	 
	// Build SQL Query  
	//$query = "SELECT * FROM adcopies WHERE body LIKE '%".$trimmed."%'"; old query to search the body only 
	$query = "SELECT * FROM adcopies WHERE body LIKE '%".$trimmed."%' OR part_name LIKE '%".$trimmed."%' OR feeder_style LIKE '%".$trimmed."%'"; 

	
	$casestudies = Db::ExecuteQuery($query, $conn);
	
	$renderpage = "<br />"; // escape first line
	$renderpage .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"left\" valign=\"top\">";
	
	foreach ($casestudies as $i => $cs){
		if($cs['id'] == "66"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		if($cs['id'] == "131"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		if($cs['id'] == "196"){
			$renderpage .= "</td><td align=\"left\" valign=\"top\">";
		}
		
		// is the ad copy out of date?
		if($cs['OBSOLETE'] == "yes"){ //yup. don't display a link to it.
			//$renderpage .= $cs['caseno'] . " - ". $cs['title'] . "<br />";
			$renderpage .= "<strong>". $cs['caseno'] . "</strong> - ". $cs['title'] . "<br /> (Part: ". $cs['part_name'] . ", Feeder Style: ". $cs['feeder_style'] . ")<br />";
		} else if($cs['OBSOLETE'] == "no"){ // nope, its still good. display a link.
			//$renderpage .= $cs['caseno'] . " - <a href=\"../../adcopy/".$cs['id']."\">". $cs['title'] . "</a><br />";
			$renderpage .= "<strong>". $cs['caseno'] . "</strong> - <a href=\"../../adcopy/".$cs['id']."\">". $cs['title'] . "</a><br /> (Part: ". $cs['part_name'] . ", Feeder Style: ". $cs['feeder_style'] . ")<br />";
		}
	
	}
	
		$renderpage .= "</td></tr></table>";
		$searchq = $var;
		$title = "Search Results for: ".$var;
	} else {
		$renderpage = "";
		$searchq = "";
		$title = "Search";
	}

$context["title"] = $title;
$context["message"] = $message;
$context["searchq"] = $searchq;
$context["body"] = $renderpage;

echo $twig->render('adcopysearch.html', $context);