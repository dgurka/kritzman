<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
/*	$name = post("name");
	$email = post("email");
	$message = post("message");
	$body = <<<EOD
Name: $name
Email: $email
Message:
$message
EOD;*/

			$name = post("name");
			$from_email = post("email");
			$subject = "voteKRITZMAN.com Contact Request";
			$message = post("message");
			
			$volunteer  = post("volunteer");
			
			$vsign = post("vsign");
			$vvolunteer  = post("vvolunteer");
			$vfacebook = post("vfacebook");
			$vphone = post("vphone");
			$vpoll = post("vpoll");
			$vdoor = post("vdoor");
			$vendorsement = post("vendorsement");
			$vdonation = post("donation");
			$vaddress = post("vaddress");
			$vcity = post("vcity");
			$vphone = post("vphone");
			
			$body = "Name: $name\n E-Mail: $from_email\n Message:\n $message\n\n";
			
			$body .= "Will Volunteer? " . $volunteer ."\n";
			
			if($volunteer == "Yes"){
				$body .= $vsign."\n";
				$body .= $vvolunteer."\n";
				$body .= $vfacebook."\n";
				$body .= $vphone."\n";
				$body .= $vpoll."\n";
				$body .= $vdoor."\n";
				$body .= $vendorsement."\n";
				$body .= $vdonation."\n";
				$body .= "Address: ". $vaddress ."\n";
				$body .= "City: ". $vcity ."\n";
				$body .= "Phone: ". $vphone ."\n";
			}
			
		mail("dgurka@enablepoint.com, enablepoint@gmail.com", "Contact Request", $body, "From: " . NO_REPLY_EMAIL);

	if(mail(ADMIN_EMAIL, "Contact Request", $body, "From: " . NO_REPLY_EMAIL ))
	{
		$context["message"] = "<h3 style=\"color:green\">Your message has been submitted!</h3>";
	}
	else
	{
		$context["message"] = "<h3 style=\"color:green\">Your message failed to send. Please try again.</h3>";
	}
}

echo $twig->render('contact.html', $context);