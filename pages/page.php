<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$id = (int)$matches[1];

$conn = Db::GetNewConnection();
$page = Db::ExecuteFirst("SELECT * FROM page WHERE ID = '$id'", $conn);
Db::CloseConnection($conn);

$context["page"] = (object)$page;

echo $twig->render('page.html', $context);