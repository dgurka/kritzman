<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

$has_day = false;

if(isset($matches[3]))
	$has_day = true;

$_month = "";
switch ($matches[2]) {
	case 1:
		$_month = "January";
		break;
	case 2:
		$_month = "February";
		break;
	case 3:
		$_month = "March";
		break;
	case 4:
		$_month = "April";
		break;
	case 5:
		$_month = "May";
		break;
	case 6:
		$_month = "June";
		break;
	case 7:
		$_month = "July";
		break;
	case 8:
		$_month = "August";
		break;
	case 9:
		$_month = "September";
		break;
	case 10:
		$_month = "October";
		break;
	case 11:
		$_month = "November";
		break;
	case 12:
		$_month = "December";
		break;
	
	default:
		# code...
		break;
}

if($has_day)
	$header = "Events on {$matches[2]}/{$matches[3]}/{$matches[1]}";
else
	$header = "Events in $_month {$matches[1]}";

if(!$has_day)
{
	$_m = $matches[2] + 1;
	$_y = $matches[1];

	if($m == 13)
	{
		$_y -= 1;
		$_m = 1;
	}
}

if($has_day)
{
	$msql = <<<EOD
SELECT 
    *
FROM
    event
WHERE
    pending = 0
        AND (`end_date` is not null
        AND `start_date` <= '{$matches[1]}-{$matches[2]}-{$matches[3]}'
        AND `end_date` >= '{$matches[1]}-{$matches[2]}-{$matches[3]}')
        OR `start_date` = '{$matches[1]}-{$matches[2]}-{$matches[3]}'
ORDER BY `start_date` , `start_time`
EOD;
	$events = Db::ExecuteQuery($msql, $conn);
}
else
	$events = Db::ExecuteQuery("SELECT * FROM event WHERE pending = 0 AND `start_date` >= '{$matches[1]}-{$matches[2]}-1' AND `start_date` < '{$_y}-{$_m}-1' ORDER BY `start_date`, `start_time`", $conn);

Db::CloseConnection($conn);

foreach ($events as $key => $value) 
{
	$events[$key]["start_date"] = disDate($events[$key]["start_date"]);
	$events[$key]["encoded_address"] = urlencode($events[$key]["location_address"]);
}

$context["header"] = $header;
$context["events"] = $events;

$context["evt_month"] = $matches[2];
$context["evt_year"] = $matches[1];

DebugWrapper::$Dbg->dump($context);

echo $twig->render('events.html', $context);