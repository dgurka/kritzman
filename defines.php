<?php

// define constants, there are seperate sets for local and live

define("WEBSITE_TITLE", "voteKRITZMAN.com");

define("PHONE_NUMBER", "(734) 237-7005");
define("PUBLIC_EMAIL", "bkritzman@sbcglobal.net");
define("STORE_ADDRESS", "Citizens for Brandon M. Kritzman<br />29601 Jacquelyn Drive<br />Livonia, Mi 48154");


define("BASE_DIR", dirname(__FILE__) . "/");
define("TEMPLATE_DIR", BASE_DIR . "templates/");
define("TEMPLATE_PACKAGE", "kritzman");
define("ADMIN_TEMPLATE_PACKAGE", "admin"); // use this in the 
define("DISPLAY_ERRORS", true);
define("ADMIN_EMAIL", "bkritzman@sbcglobal.net");
define("NO_REPLY_EMAIL", "no-reply@votekritzman.com");

$r = $_SERVER["PHP_SELF"];
$r = str_replace("index.php", "", $r);

if($r == "/")
{
	define("URI_BASE", "/");
	
	define("REQUEST_URI", ltrim($_SERVER["REQUEST_URI"], "/"));
}
else
{
	define("URI_BASE", str_replace("/", "\\/", $r));
	
	define("REQUEST_URI", preg_replace("/^" . URI_BASE . "/", "", $_SERVER["REQUEST_URI"]));
}

define("URL_ROOT", $r);

// LIVE SERVER: 
	define("DB_SERVER", "localhost");
	define("DB_USER", "kritzman");
	define("DB_PASSWORD", ".qx^OBD~D!7[");
	define("DB_DATABASE", "kritzman");

define("STATIC_ROOT", URL_ROOT . "templates/" . TEMPLATE_PACKAGE . "/"); // points to the web root of the template
define("IMAGE_ROOT", STATIC_ROOT . "images/");
define("CSS_ROOT", STATIC_ROOT . "css/");

// usage defines
define("EDITABLES_COUNT", 0); // if > 0 render editables in index.php context
define("USE_DROPDOWN", true); // if true render downdown in the context
define("MAIN_PAGE", "page/1/");
define("EVENT_CATEGORIES", "Entertainment,Community,Sports");
define("GENERAL_EVENT_NAME", "General Events");
define("USE_CACHE", false);
define("CACHE_DRIVER_ORDER", "APC,DB");
define("CAN_ADD_PAGES", true);
define("USE_DIRECTORY", false);

define("USE_GALLERY", true);
define("GALLERY_NAMES", "General");

// app use constants
require_once("app_defines.php");